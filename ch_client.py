from flask import Flask,redirect,request
import bisect
import hashlib
import mmh3
import socket
import struct

app = Flask(__name__)
#consistenthashring
class ConsistentHashRing(object):


    def __init__(self, replicas):
        
        self.replicas = replicas
        self._keys = []
        self._nodes = {}

    def _hash(self, key):
        

        return long(hashlib.md5(key).hexdigest(), 16)

    def _repl_iterator(self, nodename):
        

        return (self._hash("%s:%s" % (nodename, i))
                for i in xrange(self.replicas))

    def __setitem__(self, nodename, node):
        
        for hash_ in self._repl_iterator(nodename):
            if hash_ in self._nodes:
                raise ValueError("Node name %r is "
                            "already present" % nodename)
            self._nodes[hash_] = node
            bisect.insort(self._keys, hash_)

    def __delitem__(self, nodename):


        for hash_ in self._repl_iterator(nodename):
            # will raise KeyError for nonexistent node name
            del self._nodes[hash_]
            index = bisect.bisect_left(self._keys, hash_)
            del self._keys[index]

    def __getitem__(self, key):
       
        
        hash_ = self._hash(key)
        start = bisect.bisect(self._keys, hash_)
        if start == len(self._keys):
            start = 0
        return self._nodes[self._keys[start]]

#forpost method

@app.route("/",methods = ['POST'])
def hello():
    data = request.get_json(force=True)
    idno=cr[data["id"]]

    return redirect("http://localhost:"+str(idno)+"/v1/expenses", code=307)

#for get method

@app.route("/<int:exp_id>",methods = ['GET'])
def getrequest(exp_id):
    
    idno=cr[str(exp_id)]

    return redirect("http://localhost:"+str(idno)+"/v1/expenses/"+str(exp_id), code=302)

if __name__ == "__main__":
    cr = ConsistentHashRing(1)

    cr["node1"] = "5001"
    cr["node2"] = "5002"
    cr["node3"] = "5003"

   
    app.run(debug=True,host='0.0.0.0',port=5000)
