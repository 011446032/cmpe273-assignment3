Steps to Execute:

1. Run app1.py and map the port, database, password
2. Create Mysql docker instance1 with the docker(kinematic).. map the port, database, password to the mysql instance in the configuration.
3. Run app2.py and map the port, database, password
4. Create Mysql docker instance2 with the docker(kinematic).. map the port, database, password to the mysql instance in the configuration.
5. Run app3.py and map the port, database, password
6. Create Mysql docker instance3 with the docker(kinematic)..  map the port, database, password to the mysql instance in the configuration.
7. Run ch_client.py at port 5000
8. Check the post and get data at port 5000.



Request 1

{
    "id" : "1",
    "name" : "Foo 1",
    "email" : "foo1@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "700",
    "submit_date" : "12-10-2016"
}

Request 2

{
    "id" : "2",
    "name" : "Foo 2",
    "email" : "foo2@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "800",
    "submit_date" : "12-10-2016"
}

Request 3

{
    "id" : "3",
    "name" : "Foo 3",
    "email" : "foo3@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "1000",
    "submit_date" : "12-10-2016"
}
Request 4

{
    "id" : "4",
    "name" : "Foo 4",
    "email" : "foo4@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "1800",
    "submit_date" : "12-10-2016"
}
Request 5

{
    "id" : "5",
    "name" : "Foo 5",
    "email" : "foo5@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "8200",
    "submit_date" : "12-10-2016"
}
Request 6

{
    "id" : "6",
    "name" : "Foo 6",
    "email" : "foo6@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "2800",
    "submit_date" : "12-10-2016"
}

Request 7

{
    "id" : "7",
    "name" : "Foo 7",
    "email" : "foo7@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "900",
    "submit_date" : "12-10-2016"
}

Request 8

{
    "id" : "8",
    "name" : "Foo 8",
    "email" : "foo8@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "800",
    "submit_date" : "12-10-2016"
}

Request 9

{
    "id" : "9",
    "name" : "Foo 9",
    "email" : "foo9@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "900",
    "submit_date" : "12-10-2016"
}



Request 10

{
    "id" : "10",
    "name" : "Foo 10",
    "email" : "foo10@bar.com",
    "category" : "office supplies",
    "description" : "iPad for office use",
    "link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
    "estimated_costs" : "800",
    "submit_date" : "12-10-2016"
}